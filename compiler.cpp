#include <sstream>
#include "compiler.h"
using namespace std;

int txxxCompiler::getTerm()
{
	string token = lex.GetToken();
	int term = gr.encode (token);
	if (token == "$id")
	{
		string value = lex.GetLexeme();
		int keyword = gr.encode (value);
		if (keyword && gr.terminal(keyword))
			term = keyword;
	}
	return term;
}

int txxxCompiler::rewrite (const char *source_name)
{
	fobject.clear();
	table_for_par_and_loc.clear();

	ferror_message.clear();
	if (!prc)
	{
		ferror_message = "Internal compiler error!";
		return 1;
	}
	
	if (!lex.Begin (source_name))
	{
		ferror_message = string ("Can't open file ") + source_name;
		return 1;
    }

	name_table_init();
	ast.clear();

	enum tState { stShift, stReduce};
	vector <char> stack;
	tState state = stShift;
	int term = 1; // ������

	tSA atr;
	int rel, a, b;
   
	while (true)
	{
		switch (state)
		{
			case stShift:
				stack.push_back (term);
				ast.push_back (atr);
				
				term = getTerm();
				if (!term)
				{
					ferror_message = "Lexical error! " + lex.GetLexeme();
					goto LERROR;
				}
				
				atr = tSA(lex.GetLexeme());
				break;
				
			case stReduce: // �������� ���������� ������
				string reverse_right_part;
				a = stack.back();
				do {
					reverse_right_part += a;
					b = a;
					stack.pop_back();
					a = stack.back();
					rel = prc->rel (a, b);
				} while (rel != 1);

				// ����� ����� ����� ���������
				string s;
				gr.reduce (reverse_right_part, s);
				char left = s[0];
				
				// �������� ������ �������� ����� �����
				stack.push_back(left);

				if (PARSER_DEBUG)
					print_prod (gr, reverse_right_part, s);

				//������
				if (left == 0) // ��������� �� �������
				{ 
                    ferror_message = "Syntax error 2!";
                    goto LERROR;
				}
				
				// ��������� ��������� �����
				// ������������� ������ ������ ����� �
				// �������� ����� ����� ���������
				rel = prc->rel (a, left);
				if (rel == 0)
				{
					ferror_message = "Syntax error 3!";
					goto LERROR;
				}
				
				// ����� ������������� ������������
				int hndl = s[1];
				if (call_sem (hndl))
					goto LERROR;
					
				int k = reverse_right_part.size() - 1;
				for (int i = 0; i< k; ++i)
					ast.pop_back();

				// ��������� ������� ������������ �������
				if (stack.size() == 2 && stack[1] == (int)gr.getStart() && term == 1)
				{
					  fobject = ast.back().obj;
					  lex.End();
					  return 0;
				}
				
				if (PARSER_DEBUG)
					ast.back().print();
		}
		
		// ������� ��������� ���������
		a = stack.back();
		b = term;
		rel = prc->rel(a,b);
		if (rel == 0)
		{
			ferror_message = "Syntax error 1!";
			break;
		}
		state = (rel == 4) ? stReduce : stShift;
	}

	LERROR:
	
	// �������� � ��������� �� ������ ����� ������ � ��������
	std::ostringstream buf;
	buf << "\n Line:" << lex.GetLineCount() << " Position:" << lex.GetStartPos();
	ferror_message += buf.str();

	if (PARSER_DEBUG)
	{
		cout << "Stack: ";
		for (size_t i = 0; i < stack.size(); ++i)
			cout << " " << gr.decode (stack[i]);
		cout << " <- " << gr.decode (term) << endl;
	}
	lex.End();
	return 1;
}

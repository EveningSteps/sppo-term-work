#include <iostream>
#include <string>
#include <iomanip>
#include "precmat.h"
using namespace std;

tPrecedenceMatrix::tPrecedenceMatrix (size_t an): n(0), matr(0)
{
	matr = new char *[an];
	size_t i = 0;
	try
	{
		for (i = 0; i < an; ++i)
		  matr[i] = new char[an];
	}
	catch (std::bad_alloc)
	{
		for (int j = 0; j < i; ++j)
			delete [] matr[j];
		delete [] matr;
		matr = 0;
		throw;
	}

	n = an;
	for (i = 0; i < n; ++i)
		for (size_t j = 0; j < n; ++j) matr[i][j] = 0;
}

tPrecedenceMatrix::~tPrecedenceMatrix()
{
	if (matr)
		for (size_t i = 0; i < n; ++i) delete [] matr[i];
			delete [] matr;
}

tPrecedenceMatrix *build (const tGrammar &gr)
{
	if (!gr)
		return 0;
		
	size_t abcsz = gr.ABCsize();
	tPrecedenceMatrix *prc = new tPrecedenceMatrix (abcsz);
	
	string first, last;
	first.reserve (abcsz), last.reserve (abcsz);
           
	size_t start = gr.getStart();
	for (size_t left = start; left < abcsz; ++left)
		for (size_t ialt = 0; ialt < gr.altCount (left); ++ialt )
		{
			const string &rp = gr.rightPart (left, ialt);
			for (size_t k = 1; k < rp.length(); ++k)
			{
				size_t A = rp[k-1];
				size_t B = rp[k];
				
				// ���������� ��������� = ����� ���������,
				// �������� ����� � ������ ����� �������
				prc->matr[A][B] |= 2;

				createlast (gr, A, last);
				createfirst (gr, B, first);
				if (!gr.terminal(B))
				{
					//  B - ���������
					// ���������� ��������� < ����� �������� A � �����
					// ��������� �� FIRST(B)
					for (size_t i = 0; i < first.length(); ++i)
						prc->matr[A][first[i]] |= 1;
						
					// ���������� ��������� > ����� ����� ��������� ��
					// LAST(A) � �����������(!) �� FIRST(B)
					for (size_t j = 0; j < last.length(); ++j)
						for (size_t i = 0; i < first.length(); ++i) 
							if (gr.terminal (first[i]))
									prc->matr[last[j]][first[i]] |= 4;
				} else
				{
					// B - ��������
					// ���������� ��������� > ����� ����� ��������� ��
					// LAST(A) � �������� B
					for (size_t j = 0; j < last.length(); ++j)
									 prc->matr[last[j]][B] |= 4;
				}
			}
		}

	// ���������� ��������� < ����� ��������
	// #(������ ��� �����) � ����� ��������� ��
	// FIRST(START)
	size_t marker = 1;
	createfirst (gr, start, first);
	for (size_t i = 0; i < first.length(); ++i) 
		prc->matr[marker][first[i]] = 1;
		
	// ������������ ��������� < ����� ��������� # � START
	prc->matr[marker][start] |= 1;

	// ���������� ��������� > ����� ����� ��������� ��
	// LAST(START) � �������� #(������ ����� �������
	// �������)
	createlast (gr, start, last);
	for (size_t j = 0; j < last.length(); ++j)
		prc->matr[last[j]][marker] = 4;
		
	// ������������ ��������� > ����� ��������� START � #
	prc->matr[start][marker] |= 4;
	return prc;
}

void print_matrix (const tGrammar &gr, tPrecedenceMatrix *prc)
{
	size_t maxw = smbwidth (gr) + 1;
	if (maxw < 5) maxw = 5;

	size_t n = prc->size();
	for (size_t i = 0; i < n; ++i)
		cout << setw (maxw) << gr.decode (i) << endl;

	for (size_t i = 1; i < n; ++i)
	{
		cout << setw (maxw) << gr.decode (i);
		for (size_t j = 1; j < n; ++j)
			  cout << setw (maxw) << prc->reltostr (i, j);
		cout << endl;
	}
}

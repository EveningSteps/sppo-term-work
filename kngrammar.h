#ifndef GRAMMAR_H
#define GRAMMAR_H

//#define DEBUG
#undef DEBUG

#define EXTRA_RULES
//#undef EXTRA_RULES

#include <map>
#include <utility>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

#ifdef DEBUG
	class grammaException {};
	class eFileNotFound: public grammaException {};
	class eUnexpectedSymbol: public grammaException {};
	class eOversizedAlphabet: public grammaException {};
	class eUnexpectedEndOfFile: public grammaException {};
#endif

class tGrammar
{
	typedef vector <string>        tABC;
	typedef map <string, char>     tInverseABC;
	typedef vector <string>        tAlternatives;
	typedef vector <tAlternatives> tProductions;
	typedef map <string, string>   tReduceTable;

	tABC         Abc;
	size_t       Start;
	tInverseABC  Iabc;
	tProductions Prod;
	tReduceTable ReduceTable;

	tGrammar (const tGrammar &);
	tGrammar &operator = (const tGrammar &);

	void clear() { Abc.clear(), Start = 0, ReduceTable.clear();}

	static bool isPrefix (const char *const p, const char *const prefix);
	static char * reMake (const char *const p);
	static int myAtoi (const char *const p);
	static void chM (char *&M);

public:
	static void reverse (string &);
	tGrammar(): Start(0) {};
	
	operator bool() const   { return Start;}
	size_t getStart() const { return Start;}
	size_t ABCsize() const  { return Abc.size();}
	
	bool terminal (size_t code) const { return code < Start; }
	string decode (size_t code) const { return  Abc[code];}
	size_t encode (const string &s) const
	{
		tInverseABC::const_iterator p = Iabc.find (s);
		return p == Iabc.end() ? 0 : p->second;
	}
	
	size_t altCount (size_t left_part) const
	{
		return Prod[left_part - Start].size();
	}

	const string &rightPart (size_t left_part, size_t alt_index) const
	{
		return Prod[left_part - Start][alt_index];
	}

	void reduce (const string &h, string& value) const
	{
		tReduceTable::const_iterator p = ReduceTable.find (h);
		value = (p == ReduceTable.end()) ? string (2, 0) : p->second;
	}
	
	void loadFromFile (const char *filename);
};

#endif

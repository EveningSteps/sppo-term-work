// compiler.h
#ifndef COMPILER_H
#define COMPILER_H
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "xxxlexer.h"
#include "kngrammar.h"
#include "precmat.h"
using namespace std;

class txxxCompiler
{
public:
	txxxCompiler();
	~txxxCompiler() { delete prc;}
	int rewrite (const char *source_name);
	const string &getObject() const { return fobject;}
	const string &getMessage() const { return ferror_message;}
	bool PARSER_DEBUG;
	
private:
	txxxCompiler (const txxxCompiler &);
	txxxCompiler &operator = (const txxxCompiler &);

	txxxLexer lex;
	tGrammar gr;
	tPrecedenceMatrix *prc;
	string ferror_message;
	string fobject;

	int getTerm();
	string convertid (string id);
	string function_prototype (string name, int arity);

	// ������� ����
	// �������� �����
	enum
	{
		PROC = 1,		// ���������
		VAR  = 2,		// ����������
		GLOB = 4,		// ����������
		LOC  = 8,		// ���������
		
		// ������ ��� ���������� ����:
		DEF  = 16,		// ���������� ��� ����������
		PDEF = 32,		// ��������������
		USED = 64		// ������������ ��� ����������
	};

	struct tnmValue;
	typedef map<string, tnmValue> tNameTable;
	tNameTable fname_table;
	tNameTable table_for_par_and_loc;
	int fscope; // 0 - ����������, 1 - ���������
	void name_table_init();

	// ��������
	struct tSA;
	vector <tSA> ast; // ���� ���������

	// ������� ���������� �� ������������� �������������
	typedef int(txxxCompiler::* tSemPointer)();
	vector <tSemPointer> links;
	int call_sem (int hndl)
	{
		if (hndl < 1 || hndl >= links.size())
			return 0;
			
		tSemPointer sptr = links[hndl];
		return sptr ? (this->*sptr)() : 0; // ����� ������� ������ ����� ���������	 
	}

	int xxx01();
	int xxx02();
	int xxx04();
	int xxx05();
	int xxx07();
	int xxx08();
	int xxx09();

	int xxx14();
	int xxx15();
	int xxx19();

	int xxx20();
	int xxx21();
	int xxx22();
	int xxx23();
	int xxx26();

	int xxx32();
	int xxx35();
	int xxx36();
	int xxx37();
	int xxx39();

	int xxx43();
	int xxx46();
	int xxx47();
	int xxx48();

	int xxx50();
	int xxx51();
	int xxx52();
	int xxx53();
	int xxx54();
	int xxx55();
	int xxx56();
	int dummy();
};

struct txxxCompiler::tSA
{
	string name, obj;
	int count;
	
	tSA (string aname = string(), int acount = 0, string aobj = string()):
		obj (aobj), name (aname), count (acount) {}

	void print()
	{
		cout << "\t< " << name << "|" << count << "| " << obj << " >\n";
	}
};

struct txxxCompiler::tnmValue
{
	int properties;
	int arity; // ���������� ����������
	string alias; // ���������

	tnmValue (int  bprop = 0, int  barity = 0, string balias = string()):
		properties (bprop), arity (barity), alias (balias) {}
	bool check (int aprop) { return properties & aprop;}
	tnmValue &set (int aprop)
	{
		properties |= aprop;
		return *this;
    }
    
	tnmValue &reset (int aprop)
	{
    	properties &= ~aprop;
    	return *this;
    }
    
	tnmValue &reset()
	{
    	properties = 0;
    	return *this;
    }
};

#endif

; prime
(define (smallest-divisor n)
  (find-divisor n 2))

(define (eq a b) (cond ((not (or (< a b) (< b a))) 1) (else 0)))

(define (square x) (* x x))
(define (find-divisor n test-divisor)
  (cond ((< n (square test-divisor)) n)
        ((< 0 (divides? test-divisor n)) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b)
  (cond ((< 0 (eq (remainder b a) 0)) 1) (else 0)))

(define (prime? n)
  (cond ((< 0 (eq n (smallest-divisor n))) 1) (else 0)))

(prime? 3)
(prime? 4)
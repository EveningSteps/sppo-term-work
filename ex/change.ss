; change
(define (count-change amount)
  (cc amount 5)
)

(define (eq a b) (cond ((not (or (< a b) (< b a))) 1) (else 0)))

(define (cc amount kinds-of-coins)
  (cond ((< 0 (eq 0 amount))
           1)
        ((or (< amount 0) (< 0 (eq kinds-of-coins 0)))
           0)
        (else
           (+ (cc amount
                     (- kinds-of-coins 1))
                 (cc (- amount
                        (first-denomination kinds-of-coins))
                     kinds-of-coins))
        )
  )
)

(define (first-denomination kinds-of-coins)
  (cond ((< 0 (eq kinds-of-coins 1)) 1)
        ((< 0 (eq kinds-of-coins 2)) 5)
        ((< 0 (eq kinds-of-coins 3)) 10)
        ((< 0 (eq kinds-of-coins 4)) 25)
        (else 50)
  )
)

(count-change 100)
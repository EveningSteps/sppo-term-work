// ������ ��� ��������������� ���������
void addunique (string &strset, char a)
{
	if (strset.find_first_of (a) == string::npos)
		strset += a;
}


void add_to_first (const tGrammar &gr, char a, string &first)
{
	// a -- ����������
	for (size_t ialt = 0; ialt < gr.altCount (a); ++ialt)
		addunique (first, gr.rightPart (a, ialt)[0]);
}

void createfirst (const tGrammar &gr, char a, string &first)
{
	first.clear();
	if (gr.terminal (a))
	{
		first += a;
		return;
	}
	
	add_to_first (gr, a, first);
	for (size_t i = 0; i < first.length(); ++i)
		if (!gr.terminal (first[i]))
			add_to_first (gr, first[i], first);
}

void add_to_last (const tGrammar &gr, char a, string &last)
{
	// a -- ����������
	for (size_t ialt = 0; ialt < gr.altCount (a); ++ialt)
	{
		const string &rp = gr.rightPart (a, ialt);
		addunique (last, rp[rp.length() - 1]);
	}
}

void createlast (const tGrammar &gr, char a, string &last)
{
	last.clear();
	if (gr.terminal (a)) return;
	add_to_last (gr, a, last);
  
	for (size_t i = 0; i < last.length(); ++i)
		if (!gr.terminal (last[i]))
			add_to_last (gr, last[i], last);
}

#include "xxxlexer.h"
using namespace std;

void xxx_int (tFSM &fsm);
void xxx_num (tFSM &fsm);
void xxx_id (tFSM &fsm);
void xxx_oper (tFSM &fsm);

txxxLexer::txxxLexer(): start (buf), end (buf), line (0)
{
	*buf = 0;

	xxx_int (xxxint);
	xxx_num (xxxnum);
	xxx_id (xxxid);
	xxx_oper (xxxoper);
}

bool txxxLexer::Begin (const char *filename)
{
	line = 0;
	start = buf;
	end = buf;
	*buf = 0;
	lexeme = "";
	fsource.clear();
	fsource.open (filename);
	return fsource;
}

string txxxLexer::GetToken()
{
	lexeme = "";
	
	for (;;) // ���������� ���������� ������� � �����������
	{
		if (*end == 0 || *end == ';')
		{
			fsource.getline (buf, bufsize);
			end = buf;
			++line;
			if (!fsource)
			{
				*end = 0;
				start = end;
				return "#"; //����� �����
			}
			continue;
		}
		
		if (!std::isspace (*end)) break; // ������������ ������
		++end;
	}

	start = end;                     // ������ �������
	if (*start == '(' || *start == ')')
	{
		lexeme = string (1, *start); // ������ �� ������ �������
		++end;                       // ���������� �� ������
		return lexeme;
	}
	
	// ���� (end ������������ �� ����� �����)
	while (*(++end) != 0)
		if (std::isspace (*end) || *end == ';' || *end == '(' || *end == ')') break;
		
   lexeme = string (start, end);     // ������ �� ������������������ ��������

	// �������� ���������� ����� ������ �����
   int total = lexeme.length();
   const char *input = lexeme.c_str();
   if (xxxint.apply (input) == total) return "$int";
   if (xxxnum.apply (input) == total) return "$num";
   if (xxxid.apply (input) == total) return "$id";
   if (xxxoper.apply (input) == total) return lexeme;
   return "?";                       // ����������� �����
 }

void xxx_int (tFSM &fsm)
{
	fsm.clear();
	addstr (fsm, 0, "+-", 1);
	fsm.add (0, '0', 2);
	fsm.add (1, '0', 2);
	addrange (fsm, 1, '1', '9', 3);
	addrange (fsm, 0, '1', '9', 3);
	addrange (fsm, 3, '0', '9', 3);
	fsm.final (2);
	fsm.final (3);
}

void xxx_num (tFSM &fsm)
{
	fsm.clear();
	addstr (fsm, 0, "+-", 1);
	addrange (fsm, 0, '0', '9', 2);
	addrange (fsm, 1, '0', '9', 2);
	addrange (fsm, 2, '0', '9', 2);
	fsm.add (2, '.', 3);
	addrange (fsm, 3, '0', '9', 4);
	addrange (fsm, 4, '0', '9', 4);
	addstr (fsm, 3, "eE", 5);
	addstr (fsm, 2, "eE", 5);
	addstr (fsm, 4, "eE", 5);
	addstr (fsm, 5, "+-", 6);
	addrange (fsm, 6, '0', '9', 7);
	addrange (fsm, 5, '0', '9', 7);
	addrange (fsm, 7, '0', '9', 7);
	fsm.final (4);
	fsm.final (7);
	fsm.final (3);
}

void xxx_id (tFSM &fsm)
{
	fsm.clear();
	addstr (fsm, 0, "?!", 1);
	addrange (fsm, 0, 'a', 'z', 1);
	addrange (fsm, 0, 'A', 'Z', 1);
	addrange (fsm, 1, '0', '9', 1);
	addrange (fsm, 1, 'a', 'z', 1);
	addrange (fsm, 1, 'A', 'Z', 1);
	addstr (fsm, 1, "?!-", 1);
	fsm.final (1);
}

void xxx_oper (tFSM &fsm)
{
	fsm.clear();
	addstr (fsm, 0, "<>", 1);
	fsm.add (1, '=', 2);
	addstr (fsm, 0, "+-/*=", 2);
	fsm.final (1);
	fsm.final (2);
}

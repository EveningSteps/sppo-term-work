#include <iostream>
#include <fstream>
#include <iomanip>
#include "kngrammar.cpp"
#include "printgrn.cpp"
#include "fstlst.cpp"
#include "precmat.cpp"
#include "xxxlexer.cpp"
#include "compiler.cpp"
#include "compilerxxx.cpp"
using namespace std;

int main()
{
	cout << "MICROLISP xxxCompiler v802\n";
	txxxCompiler comp;
  
	while (true)
	{
		char source_name[100];
		cout << "\nSource file name> ";
		*source_name = 0;
		cin.getline (source_name, 100);
		if (*source_name == 0) break;
		
		char *name = source_name;
		if (comp.PARSER_DEBUG = (*name == '+')) ++name;

		string source = string (name) + ".ss";
		cout << "\nSource: " << source.c_str() << endl;
		
		{ // ���������� � �������� �����
			char linebuf[100];
			ifstream file (source.c_str());
			int linecount = 0;
			while (file)
			{
				*linebuf = 0;
				file.getline (linebuf, 100);
				cout << setw(4) << ++linecount << "|" << linebuf << endl;
			}
			 cout << "_________________\n";
		}

		int res = comp.rewrite (source.c_str());
		if (res)
			cout << comp.getMessage() <<endl;
		else
		{
			cout << "Object code: " <<endl;
			cout << comp.getObject() << endl;

			string object_name = string (name) + ".cpp";
			cout << "Would you like to save object code as "
			     << object_name << "? Y/N> ";
				 
			char ans[10];
			cin.getline (ans, 10);
			if (*ans == 'Y' || *ans == 'y' )
			{
				ofstream file (object_name.c_str());
				file << comp.getObject();
				cout << (file ? "Done!" : "Can't save!") << endl;
			}
		}
	}

	return 0;
}

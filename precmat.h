#ifndef PRECMAT_H
#define PRECMAT_H
#include <string>
#include "kngrammar.h"

class tPrecedenceMatrix
{
	size_t n;
	char **matr;
	tPrecedenceMatrix (size_t an);

public:
	~tPrecedenceMatrix();
	size_t size() const { return n;}
	int rel (int i, int j) const { return matr[i][j];}

	std::string reltostr (int i, int j) const
	{
		const char *tab[] = {"", "<", "=", "<=", ">", "<>", "=>", "<=>"}; // 0 1 2 3 4 5 6 7
		return tab[matr[i][j]];
	} 

	friend tPrecedenceMatrix* build (const tGrammar &);
};

tPrecedenceMatrix *build (const tGrammar &);
void print_matrix (const tGrammar &, tPrecedenceMatrix *);

#endif

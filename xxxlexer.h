#ifndef LEXER_H
#define LEXER_H

#include <iostream>
#include <fstream>
#include <string>
#include "fsm.h"

class txxxLexer
{
	std::ifstream fsource;            	// ����� ����� �� �����
	int line;                         	// ������� �����
	char buf[100];                    	// ����� ������
	static const size_t bufsize = 100;
	char *start;                      	// ��������� ������ ������� � ������
	char *end;                        	// ��������� ����� ������� � ������
	std::string lexeme;               	// �������

public:
	txxxLexer();
	~txxxLexer() {}
	bool Begin (const char *filename);	// ���������� � ������ � ������
	void End() {fsource.close();}      	// ���������� ������ � ������

	std::string GetToken();           	// ��������� �� ������ ��������� �����
	std::string GetLexeme() const {return lexeme;}
	int GetLineCount() const {return line;}
	int GetStartPos() const {return start-buf;}
	int GetEndPos() const {return end-buf;}
	std::string Authentication() const {return std::string ("xxx");} // �������� �� ���� ID (���)

private:                              	// �������� �������� ��� ������������� ����� (xxx -> ID)
	tFSM xxxint;
	tFSM xxxnum;
	tFSM xxxid;
	tFSM xxxoper;
};
#endif

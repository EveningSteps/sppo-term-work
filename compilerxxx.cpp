#include "compiler.h"

string txxxCompiler::convertid (string id)
{
	const char *reserved[] = {"and", "and_eq", "asm", "bitand", "bitor", "bool", "break", "case", "catch", "char", "class", "compl", "const", "const_cast", "continue", "delete", "do", "double", "dynamic_cast", "else", "enum", "explicit", "export", "extern", "false", "float", "for", "friend", "goto", "if", "inline", "int", "long", "mutable", "namespace", "new", "not", "not_eq", "operator", "or", "or_eq", "private", "protected", "public", "register", "reinterpret_cast", "return", "short", "signed", "sizeof", "size_t", "static", "static_cast", "struct", "switch", "template", "this", "throw", "true", "try", "typedef", "typeid", "typename", "union", "unsigned", "using", "virtual", "void", "volatile", "while", "xor", "xor_eq"};
	const int n = sizeof (reserved) / sizeof (char *);
	for (int i = 0; i < n; ++i)
	if (id == reserved[i])
		return string ("__") +
		lex.Authentication() +
		"__" + id;
	
	string s;
	for (int i = 0; i < (int)id.size(); ++i)
	{
		switch (id[i])
		{
			case '?': s += "_Q"; break;
			case '!': s += "_E"; break;
			case '-': s += "__"; break;
			default: s += id[i];
		}
	}
	return s;
}

string txxxCompiler::function_prototype
(string name, int arity)
{
	string str = "double " + convertid (name) + " (";
	if (arity == 0)
		str += ");\n";
	for (int i = 0; i < arity; i++)
		str += (i != arity - 1) ? "double, " : "double);\n";
	return str;
}

void txxxCompiler::name_table_init()
{
	fname_table.clear();
	
	fname_table["pi"] =
		tnmValue(VAR | GLOB | PDEF, 0, "M_PI");
	fname_table["e"] =
		tnmValue(VAR | GLOB | PDEF, 0, "M_E");
	fname_table["abs"] =
		tnmValue(PROC | GLOB | PDEF, 1, "fabs");
	fname_table["remainder"] =
		tnmValue(PROC | GLOB | PDEF, 2, "fmod");
	fname_table["exp"] =
		tnmValue(PROC | GLOB | PDEF, 1);
}

txxxCompiler::txxxCompiler(): prc(0), fscope(0)
{
	PARSER_DEBUG = false;
	
	char gramma_name[] = "lg802.txt";
	gr.loadFromFile (gramma_name);
	prc = build (gr);

	tSemPointer tmp[] = {
		0,						// 0
		&txxxCompiler::xxx01,	// 1
		&txxxCompiler::xxx02,	// 2
		&txxxCompiler::dummy,	// 3
		&txxxCompiler::xxx04,	// 4
		&txxxCompiler::xxx05,	// 5
		&txxxCompiler::dummy,	// 6
		&txxxCompiler::xxx07,	// 7
		&txxxCompiler::xxx08,	// 8
		&txxxCompiler::xxx09,	// 9
		
		&txxxCompiler::dummy,	// 10
		&txxxCompiler::dummy,	// 11
		0,						// 12
		&txxxCompiler::dummy,	// 13
		&txxxCompiler::xxx14,	// 14
		&txxxCompiler::xxx15,	// 15
		&txxxCompiler::xxx15,	// 16!
		&txxxCompiler::xxx15,	// 17!
		&txxxCompiler::xxx15,	// 18!
		&txxxCompiler::xxx19,	// 19
		
		&txxxCompiler::xxx20,	// 20
		&txxxCompiler::xxx21,	// 21
		&txxxCompiler::xxx22,	// 22
		&txxxCompiler::xxx23,	// 23
		0, 0,					// 24, 25
		&txxxCompiler::xxx26,	// 26
		0, 0, 0, 0, 0,			// 27, 28, 29, 30, 31
		
		&txxxCompiler::xxx32,	// 32
		0,						// 33
		&txxxCompiler::dummy,	// 34
		&txxxCompiler::xxx35,	// 35
		&txxxCompiler::xxx36,	// 36
		&txxxCompiler::xxx37,	// 37
		0,						// 38
		&txxxCompiler::xxx39,	// 39
		
		0, 0,					// 40, 41
		&txxxCompiler::dummy,	// 42
		&txxxCompiler::xxx43,	// 43
		0, 0,					// 44, 45
		&txxxCompiler::xxx46,	// 46
		&txxxCompiler::xxx47,	// 47
		&txxxCompiler::xxx48,	// 48
		0,						// 49
		
		&txxxCompiler::xxx50,	// 50
		&txxxCompiler::xxx51,	// 51
		&txxxCompiler::xxx52,	// 52
		&txxxCompiler::xxx53,	// 53
		&txxxCompiler::xxx54,	// 54
		&txxxCompiler::xxx55,	// 55
		&txxxCompiler::xxx56,	// 56
	};
     links = vector <tSemPointer> (tmp, tmp +
           sizeof (tmp) / sizeof (tSemPointer));
}

// EXL1 -> EXL #3
// E1 -> E #6
// E -> AREX #10
// E -> CALL #11
// E -> COND #13
// HCON -> ( cond #34
// DEFL -> DEFP #42
int txxxCompiler::dummy() { return 0;}

// S -> EXL1 #1
int txxxCompiler::xxx01()
{
	tSA &S1 = ast.back();

	string head, auth;
	head = "#include <iostream>\n";
	auth = "// compiled by MICROLISP xxxCompiler v802\n";
	bool math = false;
	tNameTable::iterator it = fname_table.begin();
	
	for (; it != fname_table.end(); ++it)
	{
		tnmValue& ref = it->second;
		if (ref.check(PDEF) && ref.check(USED)) math = true;
		if (ref.check(PROC) && ref.check(USED) && !ref.check(DEF | PDEF))
		{
			ferror_message = "ERROR: Undefined procedure: " + it->first;
			return 1;
		}
	}

	if (math) head += "#include <math.h>\n";
	S1.obj = auth + head + "\nusing namespace std;\n" + "\nint main(){\n" + 
	S1.obj + " cin.get();\n return 0;\n}\n\n";
	return 0;
}

// S -> DEFL EXL1 #2
int txxxCompiler::xxx02()
{ 
	int n = ast.size()-2;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n+1];
	string head, function_declares;
	string auth = "// compiled by MICROLISP xxxCompiler v802\n";
	bool math = true;
	math = false;
	tNameTable::iterator it = fname_table.begin();
	for(; it != fname_table.end(); ++it)
	{
		tnmValue& ref = it -> second;
		if(ref.check(PDEF) && ref.check(USED)) math = true;
		if(ref.check(PROC) && !ref.check(PDEF)) function_declares += function_prototype (it->first, ref.arity);
		if(ref.check(PROC) && ref.check(USED) && !ref.check(DEF | PDEF))
		{
			ferror_message = "ERROR: Undefined procedure " + it->first;
			return 1;
		}
	}
	
	if (math) head += "#include <math.h>\n";
	S1.obj = auth + "#include <iostream>\n" + head + "\nusing namespace std;\n" + "\n" + function_declares + "\n" + 
	S1.obj +  "\nint main(){\n" + S2.obj + " cin.get();\n	return 0;\n}\n\n";
	return 0;  
}

// EXL -> E1 #4
int txxxCompiler::xxx04()
{ 
	tSA& S1 = ast.back();

	S1.obj = " cout << " + S1.obj + " << endl;\n";
	S1.name.clear();
	S1.count = 0;
	return 0;
}

// EXL -> EXL E1 #5
int txxxCompiler::xxx05()
{ 
	int n = ast.size() - 2;
	   tSA& S1 = ast[n];
	const tSA& S2 = ast[n+1];

	S1.obj += " cout << " + S2.obj + " << endl;\n";
	return 0;
}

// E -> $id #7
int txxxCompiler::xxx07()
{ 
	tSA& S1 = ast.back();
	std::string name = S1.name;                                             
	tNameTable::iterator it = table_for_par_and_loc.find (name);
	if (it == table_for_par_and_loc.end())
	{
		it = fname_table.find (S1.name);
		if (it == fname_table.end()) { 
			ferror_message = "ERROR: variable " + S1.name + " is not defined";
			return 1;
		} else
		{
			tnmValue& ref = it -> second;
			if(!ref.check(VAR))
			{
				ferror_message = "ERROR: " + S1.name + " is not a name of variable";
				return 1;
			}
			ref.set (USED);
		}
		if (!(it->second).alias.empty()) name = (it->second).alias;
	}
	
	name = convertid (name);
	S1.obj = name + " ";                                                         
	return 0;
}

// E -> $int #8
int txxxCompiler::xxx08()
{ 
	tSA& S1 = ast.back();
	S1.obj = S1.name + ". ";
	return 0;
}

// E -> $num #9
int txxxCompiler::xxx09()
{  
	tSA& S1 = ast.back();
	S1.obj = S1.name;
	return 0;    
}

// AREX -> HAR E1 ) #14
int txxxCompiler::xxx14()
{ 
	int n = ast.size()-3;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n+1];

	int oper = S1.name[0];
	if (S1.count == 0)
		switch (oper)
		{
			case '+':
			case '*':
				S1.obj = S2.obj;
				break;
				
			case '-':
				S1.obj = "- (" + S2.obj + ")";
				break;
				
			case '/':
				S1.obj = "1.0 / (" + S2.obj+ ")";
				break;
		}
	else S1.obj = S1.obj + "(" + S2.obj + ")";
	S1.count = 0;
	return 0;
}

// HAR -> (- | (+ | (* | (/ #15
int txxxCompiler::xxx15()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n+1];
	S1 = S2;
	return 0;
}

// HAR -> HAR E1 #19
int txxxCompiler::xxx19()
{ 
	int n = ast.size()-2;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n+1];

	S1.obj = S1.obj + "(" + S2.obj + ")";
	S1.obj += S1.name + " ";
	++S1.count;
	return 0;
}

// BE -> HREL E1 ) #20
int txxxCompiler::xxx20()
{ 
	int n = ast.size()-3;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n+1];
	S1.obj = S1.obj + " " + S2.obj;
	return 0;
}

// BE-> ( not BE ) #21
int txxxCompiler::xxx21()
{
	int n = ast.size() - 4;
	tSA& S1 = ast[n];
	tSA& S3 = ast[n + 2];
	S1.obj += "!(" + S3.obj + ")";
	return 0;
}

// BE -> HAOR BE ) #22
int txxxCompiler::xxx22()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 1];
	if (S1.count == 0) S1.obj = "(" + S2.obj + ")";
	else S1.obj += " " + S1.name + " (" + S2.obj + ")";
	return 0;
}

// HREL -> ( < E #23
int txxxCompiler::xxx23()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 1];
	const tSA& S3 = ast[n + 2];
	S1.obj = S3.obj + " " + S2.name + " ";
	return 0;
}

// HAOR -> ( or BE #26
int txxxCompiler::xxx26()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	const tSA& S3 = ast[n + 2];
	S1.name =  "||" ;
	S1.obj = "(" + S3.obj + ")";
	S1.count = 1;
	return 0;
}

// COND -> HCON ECLA ) #32
int txxxCompiler::xxx32()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 1];
	S1.obj = "(" + S1.obj + S2.obj + ")" ;
	return 0;
}

// HCON -> HCON CLAU #35
int txxxCompiler::xxx35()
{ 
	int n = ast.size()-2;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n+1];
	S1.obj = S1.obj + S2.obj;
	return 0;
}

// CLAU -> HCLA E1 ) #36
int txxxCompiler::xxx36()
{ 
	int n = ast.size()-3;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n+1];
	S1.obj = " " + S1.obj + " ? " + S2.obj + " : \n";
	return 0;
}

// HCLA -> ( BE #37
int txxxCompiler::xxx37()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n+1];
	S1.obj = S2.obj;
	return 0;
}

// ECLA -> ( else E1 ) #39
int txxxCompiler::xxx39()
{
	int n = ast.size() - 4;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 2];
	S1 = S2;
	return 0;
}

// DEFL -> DEFL DEFP #43
int txxxCompiler::xxx43()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 1];
	S1.obj += S2.obj; 
	return 0;
}

// DEFP -> HDEF E1 ) #46
int txxxCompiler::xxx46()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n + 1];
	S1.obj = S1.obj + " return " + S2.obj + ";\n}\n";
	tNameTable::iterator it = fname_table.find (S1.name);
	table_for_par_and_loc.clear();
	fscope = 0;
	
	if (it == fname_table.end())
		fname_table[S1.name] = tnmValue(PROC|DEF, S1.count);
	else
	{
		tnmValue& ref = it -> second;
		if (ref.check(PROC) && !ref.check(DEF|PDEF))
		{
			if (ref.arity != S1.count)
			{
				ferror_message = "ERROR: number of parameteres and number of arguments are not equal";
				return 1; 
			}
			fname_table[S1.name] = tnmValue (PROC|DEF, S1.count);                  
			return 0;
		} 
		ferror_message = "ERROR: another procedure or variable has name " + S1.name;
		return 1;
	}
	return 0;
}

// HDEF -> HIDL ) #47
int txxxCompiler::xxx47()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];
	S1.obj = S1.obj + "){\n";
	return 0;  
}

// HDEF -> HDEF DEFV #48
int txxxCompiler::xxx48()
{ 
	int n = ast.size()-2;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n + 1];
	S1.obj = S1.obj + S2.obj;
	return 0;
}

// HIDL -> ( define ( $id #50
int txxxCompiler::xxx50()
{ 
	int n = ast.size()-4;
	fscope = 1;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n+3];
	S1.count = 0;
	S1.obj = "\ndouble " + convertid (S2.name) + "(";
	S1.name = S2.name;
	return 0;
}

// HIDL -> HIDL $id #51
int txxxCompiler::xxx51()
{ 
	int n = ast.size()-2;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n + 1];
	if (S1.count != 0)
		S1.obj = S1.obj + ", " + "double " + convertid (S2.name);
	else S1.obj = S1.obj + "double " + convertid (S2.name);
	
	S1.count++;
	tNameTable::iterator it;	
	it = table_for_par_and_loc.find (S2.name);
	if (it != table_for_par_and_loc.end())
	{
		ferror_message = "ERROR: cannot redefine parameter";
		return 1;
	}
	table_for_par_and_loc[S2.name] = tnmValue (VAR|LOC);
	return 0;
}

// DEFV -> HDFV E1 ) #52
int txxxCompiler::xxx52()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n+1];
	S1.obj += S2.obj + ";\n";
	return 0;
}

// HDFV -> ( define $id #53
int txxxCompiler::xxx53()
{ 
	int n = ast.size() - 3;
	tSA& S1 = ast[n];
	tSA& S2 = ast[n + 2];
	tNameTable::iterator it;
	
	it = table_for_par_and_loc.find (S2.name);
	if (it != table_for_par_and_loc.end())
	{
		ferror_message = "ERROR: a parameter or another local variable has name " + S2.name;
		return 1;
	}
	table_for_par_and_loc[S2.name] = tnmValue(VAR|LOC);
	S1.obj = " double " + convertid (S2.name) + " = ";
	return 0;
}

// CALL -> HCAL ) #54
int txxxCompiler::xxx54()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];

	string name = S1.name;
	int count= S1.count;
	do
	{
		tNameTable::iterator it = fname_table.find (name);
		if (it == fname_table.end())
		{
			fname_table[name] = tnmValue (PROC|GLOB|USED, count);
			break;
		}
		tnmValue& ref = it->second;

		if (ref.arity != count)
		{
			std::ostringstream buf;
			buf << "ERROR: procedure " << name
			    << " expects " << ref.arity
				<< " argument" << (ref.arity == 1 ? "" : "s")
				<< ", given " << count;
			ferror_message = buf.str();
			return 1;
		}
		ref.set (USED);
	} while (false);
   
	S1.obj += ")";
	S1.count = 0;
	return 0;
}

// HCAL -> ( $id #55
int txxxCompiler::xxx55()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 1];
	string name = S2.name;
	S1.name = name;
	
	tNameTable::iterator it;
	it = table_for_par_and_loc.find (name);
	if (it != table_for_par_and_loc.end())
	{
		ferror_message = "ERROR: variable " + S1.name + " hides a procedure";
		return 1;
    }
	
    it = fname_table.find (name);
	if (it != fname_table.end())
	{
		tnmValue& ref = it -> second;
		if (!ref.check (PROC))
		{
			ferror_message = "ERROR: " +
			name + " is not a procedure";
			return 1;
		}
		if (!ref.alias.empty())
			name = ref.alias;
	}
	
	S1.obj = convertid (name) + "(";
	return 0;
}

// HCAL -> HCAL E #56
int txxxCompiler::xxx56()
{ 
	int n = ast.size() - 2;
	tSA& S1 = ast[n];
	const tSA& S2 = ast[n + 1];
	if (S1.count) S1.obj += ", ";
	
	S1.obj += S2.obj;
	++S1.count;
	return 0;
}

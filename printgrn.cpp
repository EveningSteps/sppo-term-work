#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

void print_prod (const tGrammar &gr, const string &reverse_right_part, const string &s, size_t maxw=0)
{
	if (maxw < 5) maxw = 5;
	int left_part = s[0];
	cout << setw(maxw) << gr.decode (left_part) << " -> ";
	
	for (int i = reverse_right_part.length() - 1; i >= 0; --i)
		cout << gr.decode (reverse_right_part[i]) << " ";
	cout << gr.decode(1) << (int)s[1];
	cout << endl; 
}

void print_descr (const tGrammar &gr, const string &rp)
{
	string rev = rp;
	tGrammar::reverse (rev);
	string s;
	gr.reduce (rev, s);
	
	int descr = s[1];
	if (descr)
		cout << " " << gr.decode(1) << descr;
}

size_t smbwidth (const tGrammar &gr)
{
	size_t wmax = 0;
	for (size_t i = 1; i < gr.ABCsize(); ++i)
	{
		size_t wi = gr.decode(i).size();
		if (wi > wmax) wmax = wi;
	}
	return wmax;
}

void print_chain (const tGrammar &gr, const string &rp)
{
    for (size_t i = 0; i < rp.length(); ++i)
		cout << " " << gr.decode (rp[i]);
}

void printgrn (const tGrammar &gr)
{
	if (!gr)
	{
		cout << "??????????" << endl;
		return;
	};

	size_t wmax = smbwidth (gr);
	cout << gr.decode (1) << endl;
	for (size_t i = 2, lc = 0; i < gr.getStart(); ++i)
	{
		cout << setw (wmax) << gr.decode (i) << " ";
		if (++lc > 3)
		{
			lc = 0;
			cout << endl;
		};
	}
	cout << "\n" << gr.decode (1) << endl;

	for (size_t left = gr.getStart(); left < gr.ABCsize(); ++left)
	{
		string sleft = gr.decode (left);
		cout << setw (wmax) << sleft << " -> ";
		size_t ac = gr.altCount (left), ac1 = ac - 1;
		for (size_t ialt = 0; ialt < ac; ++ialt)
		{
			const string &rp = gr.rightPart (left, ialt);
			print_chain (gr, rp);
			print_descr (gr, rp);
			if (ialt != ac1)
				cout << " |\n" << setw (wmax) << string() << "   ";
		}
		cout << endl;
	}
	cout << endl;
}

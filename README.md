# Chup. Here lies my term work, which you are free to use for your own needs.
# Some details:
#
# # 802
# S -> EXL1 #1 | DEFL EXL1 #2
# EXL1 -> EXL #3
# EXL -> E1 #4 | EXL E1 #5
# 
# E1 -> E #6
# E -> $id #7 | $int #8 | $num #9 | AREX #10 | CALL #11 | COND #13
# 
# AREX -> HAR E1 ) #14
# HAR -> ( - #15 | ( + #16 | ( * #17 | ( / #18 | HAR E1 #19
# 
# BE ->  HREL E1 ) #20 | ( not BE ) #21 | HAOR BE ) #22
# HREL -> ( < E #23
# HAOR -> ( or BE #26
# 
# COND -> HCON ECLA ) #32
# HCON -> ( cond #34 | HCON CLAU #35
# CLAU -> HCLA E1 ) #36
# HCLA -> ( BE #37
# ECLA -> ( else E1 ) #39
# 
# DEFL -> DEFP #42 | DEFL DEFP #43
# DEFP -> HDEF E1 ) #46
# HDEF -> HIDL ) #47 | HDEF DEFV #48
# HIDL -> ( define ( $id  #50 | HIDL $id #51
# 
# DEFV -> HDFV E1 ) #52
# HDFV -> ( define $id #53
# 
# CALL -> HCAL ) #54
# HCAL -> ( $id #55 | HCAL E #56

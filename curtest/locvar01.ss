;locvar01
(define (f x)
  (define abs (* x e))
  abs)
(f e)
;locvar02
(define (f x)
  (define abs (abs(- x e)))
  abs)
(f e)
;locvar03
(define (f x)
  (define x(+ e e))
  x)
(f e)

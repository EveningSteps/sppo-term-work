#include "kngrammar.h"
#include <cstring>

void tGrammar::reverse (string &s)
{
	size_t n = s.length(), nd = n - 1, nh = n / 2;
	for (size_t i = 0; i < nh; ++i)
	{
		char c = s[i];
		s[i] = s[nd - i];
		s[nd - i] = c;
	}
}

bool tGrammar::isPrefix (const char *const p, const char *const prefix)
{
	size_t prefl = strlen (prefix);
	if (strlen (p) < prefl) return false;
	
	size_t i = 0;
	while (prefix[i] && p[i] == prefix[i]) i++;
	
	return i == prefl;
}

char *tGrammar::reMake (const char *const p)
{
	size_t r = 0;
	char *rez = new char[strlen (p)];
	for (size_t i = 0; i <= strlen (p); ++i)
	{
		rez[i - r] = p[i];
		if (p[i] == '|' && p[i + 1] != '|')
		{
			delete [] rez;
			rez = NULL;
			return rez;
		}
		
		if (p[i] == '|' && p[i + 1] == '|')
			++r,++i;
	}
	return rez;
}

int tGrammar::myAtoi (const char *const p)
{
	if (p == NULL) return -1;
	int rez = 0;
	size_t pl = strlen (p);
	
	if (pl > 3) return -1;
	for (size_t i = 0; i < pl; ++i)
	{
		if (isdigit (p[i]))
			rez = rez * 10 + (p[i]-'0');
		else return -1;
	}
	return rez;
}

void tGrammar::chM (char *&M)
{
	string r = "";
	size_t q = strlen (M);
	for (size_t i = 0; i < q; ++i)
	{
		r += M[i];
		if (M[i] == '|')
			r += '|';
	}
	delete [] M;
	M = strdup (r.c_str());
}


void tGrammar::loadFromFile (const char *filename)
{
	const int buffSize = 300;
	const char ABCdelimiters[] = " \t\n\r";
	const char PRODdelimiters[] = " \t\r";

	// �������������
	Start = 0;
	Abc.clear();
	Iabc.clear();
	Prod.clear();
	ReduceTable.clear();

	// �������� ��������
	ifstream inputF (filename);
	if (!inputF.is_open())
	{
		#ifdef DEBUG
			throw eFileNotFound();
		#endif
		return;
	}

	char *MARKER = NULL, *p = NULL;
	char str[buffSize];

	// ���������� ��� ������ ������ � ������, ��������� ������ �� �������� ������ ������ �� ������� �����
	str[0] = '\0';
	while ((!strlen(str) || ((p = strtok (str, ABCdelimiters)) == NULL)) && !inputF.eof())
		inputF.getline (str, buffSize);

	// ���� ���� ����
	if (p == NULL)
	{
		#ifdef DEBUG
			throw eUnexpectedEndOfFile();
		#endif
		return;
	}

	MARKER = strdup (p);
	Abc.push_back ("");
	Abc.push_back (MARKER);

	// �������� �������� �� �����
	p = strtok (NULL, ABCdelimiters);
	int markerCount = 1;
	while (markerCount < 2)
	{
		while((p != NULL) && (strcmp (MARKER, p) != 0))
		{
			// �������� �������� �� ������������
			tABC::const_iterator ip = find (Abc.begin(), Abc.end(), p);
			if (ip == Abc.end())
				Abc.push_back (p);
			else
			{
				clear();
				delete[] MARKER; inputF.close();
				#ifdef DEBUG
					throw eUnexpectedSymbol();
				#endif
				return;
			}
			p = strtok (NULL, ABCdelimiters);
		}
		
		if (p == NULL)
		{
			// p == NULL, �.�. � ������� ������ ���������� ������ ���
			if (!inputF.eof())
			{
				inputF.getline (str, buffSize);
				p = strtok (str, ABCdelimiters);
			} else
			{
				// ���� ������ �� ���������� ������ ���, � ���� ������� ���������, ����� ��� ������
				clear();
				delete[] MARKER; inputF.close();
				#ifdef DEBUG
					throw eUnexpectedEndOfFile();
				#endif
				return;
			}
			
		} else
		{
			// � ��������� ������, ������� �������� -- ������
			++markerCount;
			p = strtok (NULL, ABCdelimiters);
			if (p != NULL)
			{
				// � ������� ������ ����� ������� ���� ��� ���-��!
				clear();
				delete[] MARKER; inputF.close();
				#ifdef DEBUG
					throw eUnexpectedSymbol();
				#endif
				return;
			}
		}
	}

	// ���������� �������� -- �������� ������ ������������
	Start = Abc.size();
	str[0] = '\0';
	bool broken = false;
	// broken = true, ���� � ��������� ������ ���������	����������� �������� ������ �����������


	while (!inputF.eof())
	{
		inputF.getline (str, buffSize);
		
		// ����� ���������� ��������������� �������
		while ((!strlen (str) || ((p = strtok (str, PRODdelimiters)) == NULL )) && !inputF.eof())
			inputF.getline (str, buffSize);

		if (p == NULL)
			break;

		if (!broken)
		{
			// ������� ������ - ����� ������ ���������; �������� �������� �� ������������
			char *e = reMake (p);
			if (e == NULL)
			{
				delete[] MARKER; inputF.close();
				clear();
				#ifdef DEBUG
					throw eUnexpectedSymbol();
				#endif
				return;
			}
			
			tABC::const_iterator ip = find (Abc.begin(), Abc.end(), e);
			if (ip == Abc.end())
			{
				Abc.push_back (e);
				delete[] e;
			} else
			{
				// ������  ������  ��� ����! ��������, � �������� ������ ������� ������������ ����������!
				delete[] MARKER; inputF.close();
				delete[] e;
				clear();
				#ifdef DEBUG
					throw eUnexpectedSymbol();
				#endif
				return;
			}
		}

		// ���� � ����� ������ ��������� "������ + |", ������ ������ ��������� �������������.
		// ����������� ������ ����� ����������
		broken = false;

		// ��� ���������� ����� ������ (��� ������������ ��������� ������)
		while (p != NULL)
		{
			if (strcmp (p, "|"))
				broken = false;
			else broken = true;
			
			p = strtok (NULL, PRODdelimiters);
		}
	}

	// ���� ������ ����������� �������, �� ��� ������
	if (broken)
	{
		delete[] MARKER; inputF.close();
		clear();
		#ifdef DEBUG
			throw eUnexpectedEndOfFile();
		#endif
		return;
	}

	// ���� �� ���� ��������� �� ������ �����������, ��� ������
	if (ABCsize() == Start)
	{
		clear();
		delete[] MARKER; inputF.close();
		#ifdef DEBUG
			throw eUnexpectedEndOfFile();
		#endif
		return;
	}

	if (ABCsize() > 128)
	{
		clear();
		delete[] MARKER; inputF.close();
		#ifdef DEBUG
			throw eOversizedAlphabet();
		#endif
		return;
	}


	// ��������� ������ ��������� (������ ������)
	inputF.close();
	inputF.clear();
	inputF.open (filename, ios_base::in);
	markerCount = 0;
	
	int left = Start;
	string value = "xx";
	value[0] = Start;
	tGrammar::tAlternatives alt;
	bool active;

	// �������� ���������� ���� �������
	while (markerCount < 2)
	{
		str[0] = '\0';
		while ((!strlen(str) || ((p = strtok (str, ABCdelimiters)) == NULL )) && !inputF.eof())
			  inputF.getline (str, buffSize);
			  
		while (p != NULL)
		{
			if (strcmp (p, MARKER) == 0)
				++markerCount;
			p = strtok (NULL, ABCdelimiters);
		}
	}

	// ���������� ������ ���������
	chM (MARKER);
	while (!inputF.eof())
	{
		str[0] = '\0';
		alt.clear();

		// ��������� � ��������� ���������
		while ((!strlen (str) || ((p = strtok (str, PRODdelimiters)) == NULL )) && !inputF.eof())
			  inputF.getline(str,buffSize);
		// �������������� �������� �� �����, ���  ���  ���  ����  ���������  ��� ������ ������������

		// ���� ���� ����������, �� ����� �� �����
		if (p == NULL)
			break;

		p = strtok (NULL, PRODdelimiters);

		if (p == NULL || !isPrefix (p, "->"))
		{
			clear();
			delete[] MARKER; inputF.close();
			#ifdef DEBUG
				throw eUnexpectedSymbol();
			#endif
			return;
		}

		// ������ ������������
		if (strcmp(p,"->"))
			p += 2;
		else p = strtok (NULL, PRODdelimiters);

		active = true;
		string currentAlternative;

		while (active)
		{
			while (p != NULL && strcmp (p, "|") && !isPrefix (p, MARKER))
			{
				// ���� �� ���������� ������ | ��� ������
				char *e = reMake (p);
				if (e == NULL)
				{
					delete[] MARKER; inputF.close();
					clear();
					#ifdef DEBUG
						throw eUnexpectedSymbol();
					#endif
					return;
				}
				
				tABC::const_iterator ip = find (Abc.begin(), Abc.end(), e);
				delete[] e;
				if (ip == Abc.end())
				{
					// �������� ������� ��� � ��������
					clear();
					delete[] MARKER; inputF.close();
					#ifdef DEBUG
						throw eUnexpectedSymbol();
					#endif
					return;
				}
				int r = ip - Abc.begin();
				currentAlternative += (char)r;
				p = strtok (NULL, PRODdelimiters);
			}
			
			if (p == NULL)
			{
				// ��������� ������ ������ ������� �� |, �� ����
				// ������ ���� ����������� ��� ������ ����� ����� ���������
				
				value[1] = 0;
				if (currentAlternative.length())
				{
					#ifdef EXTRA_RULES
						if (currentAlternative.length() == 1 && currentAlternative[0] == left)
						{
							// ���������� ��������� ��� �� ���� [ I -> I ]
							clear();
							delete[] MARKER; inputF.close();
							#ifdef DEBUG
								throw eUnexpectedSymbol();
							#endif
							return;
						}
					#endif
					
					alt.push_back (currentAlternative);
					reverse (currentAlternative);
					if (ReduceTable.find (currentAlternative) != ReduceTable.end())
					{
						// ������ ������ ����� ��� ����������, ��� ������������ �������� ����������
						clear();
						delete[] MARKER; inputF.close();
						#ifdef DEBUG
							throw eUnexpectedSymbol();
						#endif
						return;
					}
					ReduceTable[currentAlternative] = value;
					currentAlternative = "";
				} else
				{
					clear();
					delete[] MARKER; inputF.close();
					#ifdef DEBUG
						throw eUnexpectedSymbol();
					#endif
					return;
				}
				active = false;
				
			} else
			{
				value[1] = 0;
				if (isPrefix (p, MARKER))
				{
					// ����� ������� ���������� �������������� ��������������

					int z;
					if (strcmp (p, MARKER))
					{
						// p �������� ������
						p += strlen (MARKER);
					} else
					{
						// � ����� �������
						p = strtok (NULL, PRODdelimiters);
					}
					
					z = myAtoi (p);
					if (z > 127 || z < 0)
					{
						clear();
						delete[] MARKER; inputF.close();
						#ifdef DEBUG
							throw eUnexpectedSymbol();
						#endif
						return;
					}
					
					value[1] = z;
					p = strtok (NULL, PRODdelimiters);
					if (p != NULL && strcmp(p, "|"))
					{
						// ����� ��� ������ ������� �� |
						clear();
						delete[] MARKER; inputF.close();
						#ifdef DEBUG
							throw eUnexpectedSymbol();
						#endif
						return;
					}
				}

				// p == "|" ��� NULL
				// ��������� � ������ �����������
				
				if (currentAlternative.length())
				{
					#ifdef EXTRA_RULES
						if (currentAlternative.length() == 1 && currentAlternative[0] == left)
						{
							// ���������� ��������� ��� �� ���� [ I -> I ]
							clear();
							delete[] MARKER; inputF.close();
							#ifdef DEBUG
								throw eUnexpectedSymbol();
							#endif
							return;
						}
					#endif
					alt.push_back (currentAlternative);
					reverse (currentAlternative);
					if (ReduceTable.find (currentAlternative) != ReduceTable.end())
					{
						// ������ ������ ����� ��� ����������, ��� ������������ �������� ����������
						clear();
						delete[] MARKER; inputF.close();
						#ifdef DEBUG
							throw eUnexpectedSymbol();
						#endif
						return;
					}
					ReduceTable[currentAlternative] = value;
					currentAlternative = "";
				} else
				{
					clear();
					delete[] MARKER; inputF.close();
					#ifdef DEBUG
						throw eUnexpectedSymbol();
					#endif
					return;
				}

				if (p == NULL)
					active = false;
				else
				{
					// �������� ��������� ������
					p = strtok (NULL, PRODdelimiters);
					if (p == NULL)
					{
						// ������ ����������� ����������� - ������������� ������ ������
						str[0] = '\0';
						while ((!strlen (str) || ((p = strtok (str, PRODdelimiters)) == NULL)) && !inputF.eof())
							inputF.getline(str,buffSize);
					}
					
					active = true;
				}
			}
		}
		
		if (alt.size())
		{
			Prod.push_back (alt);
			value[0] = (char)++left;
		} else
		{
			clear();
			delete[] MARKER; inputF.close();
			#ifdef DEBUG
				throw eUnexpectedSymbol();
			#endif
			return;
		}
	}

	inputF.close();
	delete[] MARKER;

	for (size_t i = 1; i < Abc.size(); ++i)
		Iabc [decode (i)] = i;

	return;
}

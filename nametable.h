#ifndef NAMETAB_H
#define NAMETAB_H
using namespace std;
// �������� �����
enum
{
	PROC = 1,  // ���������
	VAR  = 2,  // ����������
	GLOB = 4,  // ����������
	LOC  = 8,  // ���������
	
	// ������ ��� ���������� ����:
	DEF  = 16, // ���������� ��� ����������
	PDEF = 32, // ��������������
	USED = 64  // ������������ ��� ����������
};

struct tnmValue
{
	int  properties;
	int  arity;   // ���������� ���������� 
	string alias; // ���������
	
	tnmValue (int bprop = 0, int barity = 0, string balias = string()):
		properties (bprop), arity (barity), alias (balias) {}

	bool check (int aprop)
	{
		return properties & aprop;
	}
	
	tnmValue &set (int aprop)
	{
		properties |= aprop;
		return *this;
	}
	
	tnmValue &reset (int aprop)
	{
		properties &= ~aprop;
		return *this;
	}
	
	tnmValue& reset()
	{
		properties = 0;
		return *this;
	}
};

typedef map <string, tnmValue> tNameTable;
tNameTable fname_table;           // ����� ������� ����
tNameTable table_for_par_and_loc; // ������� ��������� ���������� � ����������

#endif
